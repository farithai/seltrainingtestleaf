package wdMethods;



import java.io.IOException;
import readXL.ReadXLData;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ProjectMethods extends SeMethods{
	
	public String filePath;
	
	@Parameters({"url"})
	@BeforeMethod(groups= {"any"})
	public void login(String url) {
		beforeMethod();
		startApp("chrome", url);
/*		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);*/
		
		
	}
	
	@AfterMethod(groups= {"any"})
	public void closeApp() {
		closeBrowser();
		}
	
	@BeforeSuite(groups= {"any"})
	public void beforeSuite() {
	startResult();
	}
	
	@AfterSuite(groups= {"any"})
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name="qa")
	public Object[][] fetchData() throws IOException {
		
		Object[][] readData = ReadXLData.readData(filePath);
		return readData;
		

	}
	
	
	
}












