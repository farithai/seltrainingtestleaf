package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

import util.Reporter;

public class SeMethods extends Reporter implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;


	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				
				ChromeOptions option = new ChromeOptions();
	//			option.setHeadless(true);
				option.addArguments("--disable-notifications");				
				driver = new ChromeDriver(option);
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//			reportStep("The Browser "+browser+" Launched Successfully");
			reportStep("pass", "The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
//			reportStep("The Browser "+browser+" not Launched ");
			reportStep("fail", "The Browser "+browser+" not Launched");
		} finally {
//			takeSnap();			
		}
	}

	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "LinkText": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);
			case "tag": return driver.findElementByTagName(locValue);
			case "partialLinkText": return driver.findElementByPartialLinkText(locValue);
			}
		} catch (WebDriverException e) {
//			reportStep("The Element Is Not Located ");
			reportStep("fail", "Element is not located");
		}
		return null;
	}
	@Override
	public WebElement locateElement(String locValue) {
		try {
		return driver.findElementById(locValue);
		}catch(WebDriverException e) {
			reportStep("fail", "Element is not located");
		}
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
		ele.sendKeys(data);
//		reportStep("The Data "+data+" is Entered Successfully");
		reportStep("pass", data+" is entered successfully");
		takeSnap();
		}catch(WebDriverException e) {
			reportStep("fail", "step failed - not able to enter "+data);
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
		ele.click();
//		reportStep("The Element "+ele+" Clicked Successfully");
		reportStep("pass", "Element is clicked/submitted successfully");
		}catch(WebDriverException e) {
		reportStep("fail", "Step Failed - Not able to submit/click");
		}finally {
//			takeSnap();
		}
	}
	
	public void clickWithoutSnap(WebElement ele) {
		try {
		ele.click();
		String text = ele.getText();
//		reportStep("The Element "+ele+" Clicked Successfully");
		reportStep("pass", "Button is clicked/submitted successfully");
		}catch(WebDriverException e) {
			reportStep("fail", "Step Failed - Not able to submit/click");	
		}

		
	}

	@Override
	public String getText(WebElement ele) {
		try {
		String text = ele.getText();
//		reportStep("The text present in ele is " +text);
		reportStep("pass", "The text present in element is " +text);
		return text;
		}catch(WebDriverException e) {
			reportStep("fail", "Element not found");
		}
		return null;
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
//		reportStep("The DropDown Is Selected with "+value);
		reportStep("pass", "The DropDown Is Selected with "+value);
		}catch(WebDriverException e) {
			reportStep("fail", "The DropDown Is not Selected");	
		}finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
		Select dd = new Select(ele);
		dd.selectByIndex(index);
//		reportStep("The DropDown Is Selected with "+index);
		reportStep("pass", "The DropDown Is Selected with "+index);
		}catch(WebDriverException e) {
			reportStep("fail", "The DropDown Is Not Selected");
		}finally {
		takeSnap();
		}
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String title = driver.getTitle();
		if(title.equals(expectedTitle)) {
//			reportStep(title+" is launched successfully");
			reportStep("pass", title+" is launched successfully");
			takeSnap();
			return true;
		}
		else {
			reportStep("fail", expectedTitle+" is NOT found");
			return false;
		}
			
		
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		
		boolean bReturn = false;
		String text = ele.getText();
		if(text.equalsIgnoreCase(expectedText)) {
			reportStep("pass", "Matching the given text");
			bReturn = true;
		}
		else {
			reportStep("fail", "Not matching with the given text");
			
		}
		return bReturn;

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText)) {
			reportStep("pass", "Partially matching the given text");
		}
		else {
			reportStep("fail", "Not matching with the given text");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String actualValue = ele.getAttribute(attribute);
		if(actualValue.equalsIgnoreCase(value)) {
			reportStep("pass", "Attribute is matched exactly");
		}
		else {
			reportStep("fail", "Attribute is not matched exactly");
		}


	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String actualValue = ele.getAttribute(attribute);
		if(actualValue.contains(value)) {
			reportStep("pass", "Given value is found in attribute");
		}
		else {
			reportStep("fail", "Given value is not found in attribute");
		}

	}

	@Override
	public boolean verifySelected(WebElement ele) {
		boolean verify = ele.isSelected();
		if(verify) {
			reportStep("pass", "The element "+ ele.getText()+ " is selected");
			takeSnap();
			return true;
		}
		else {
			reportStep("fail", "The element "+ele.getText()+" is not selected");
			takeSnap();
			return false;
		}
	
	

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		boolean verify = ele.isDisplayed();
		if(verify) {
			reportStep("pass", "The element "+ ele+ " is displayed");
		}
		else {
			reportStep("fail", "The element "+ele+" is not displayed");
		}
		takeSnap();

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		try{
			System.out.println(listOfWindow.size());
			driver.switchTo().window(listOfWindow.get(index));
			reportStep("pass", "The Window is Switched ");
		}catch(WebDriverException e){
			reportStep("fail", "Window is not switched");
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		driver.switchTo().frame(ele);
		reportStep("pass", "Control is moved to the frame");
		}catch(NoSuchFrameException e) {
			reportStep("fail", "Frame is not available");
		}
		takeSnap();

	}

	@Override
	public void acceptAlert() {
		try {
		driver.switchTo().alert().accept();
		reportStep("pass", "Alert message is confirmed");
		}catch(WebDriverException e) {
			reportStep("fail", "Alert message not handled");
		}
//		takeSnap();
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().accept();
			reportStep("pass", "Alert message is cancelled");
			takeSnap();
			}catch(WebDriverException e) {
				reportStep("fail", "Alert message not handled");
			}

	}

	@Override
	public String getAlertText() {
		try {
		String text = driver.switchTo().alert().getText();
		reportStep("pass", "Alert message displayed is: "+text);
		return text;
		}catch(NoAlertPresentException e) {
			reportStep("fail", "Alert is already handled. Can't read the alert message");
		}
		return null;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
			reportStep("pass", "Screenshot is captured");
		} catch (IOException e) {
			reportStep("fail", "Error in capturing the screenshot");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		try {
		driver.close();
		reportStep("pass", "Browser is closed successfully");
		}catch(WebDriverException e) {
			reportStep("fail", "No active window to close");
		}

	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
			reportStep("pass", "All open browser(s) are closed successfully");
			}catch(WebDriverException e) {
				reportStep("fail", "No active window(s) to close");
			}

	}

}
