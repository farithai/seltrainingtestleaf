package readXL;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadXLData {

	public static Object[][] readData(String filePath) throws IOException {
		XSSFWorkbook wb= new XSSFWorkbook("./data/"+filePath+".xlsx");
		
		
		XSSFSheet sheet = wb.getSheetAt(0);
		
		int cellCount = sheet.getRow(0).getLastCellNum();
		
		int rowCount = sheet.getLastRowNum();
		
		Object[][] data=new Object[rowCount][cellCount];
		
		for(int i=1;i<=rowCount;i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j=0;j<cellCount; j++) {
				try {
				XSSFCell cell = row.getCell(j);
				String cellValue = cell.getStringCellValue();
				System.out.println(cellValue);
				data[i-1][j]=cellValue;
				}catch(NullPointerException e) {
					System.out.println("NULL");
				}
			}
			
		}
		
		return data;
		
	}

}
