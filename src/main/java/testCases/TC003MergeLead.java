package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import pages.MergeLeadPage;
import wdMethods.ProjectMethods;

public class TC003MergeLead extends ProjectMethods{
	
	@BeforeTest(groups= {"sanity"})
	public void setData() {
		testName = "TC003_MergeLeads";
		testDesc = "Merge two leads";
		author = "fariti";
		category = "smoke";
		filePath="MergeLeadData";
	}
	
	@Test(dataProvider="qa")
	public void mergeLead(String userName, String password, String compName1, String fName1, String lName1, String compName2, String fName2, String lName2) throws InterruptedException {
		 new LoginPage()
			.enterUserName(userName)
			.enterPassword(password)
			.clickLogin()
			.clickCRMSFA()
			.clickLeadsButton()
			.clickMergeLead()
			.clickFromLeadIcon()
			.enterCompanyNameMerge(compName1)
			.enterFirstNameMerge(fName1)
			.enterLastNameMerge(lName1)
			.clickFindLeads()
			.clickLeadIDWindow()
			.clickToLeadIcon()
			.enterCompanyNameMerge(compName2)
			.enterFirstNameMerge(fName2)
			.enterLastNameMerge(lName2)
			.clickFindLeads()
			.clickLeadIDWindow()
			.clickMergeButton()
			.handleAlert();	
		
	}

}
