package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002CreateLead extends ProjectMethods{

	@BeforeTest(groups= {"smoke"})
	public void setData() {
		testName = "TC002_CreateLeads";
		testDesc = "Create A new Lead";
		author = "fariti";
		category = "smoke";
		filePath="createLeadData";
	}
	
	@Test(dataProvider="qa")
	public void createLead(String userName, String password, String compName, String firstName, String lastName) {
		new LoginPage()
			.enterUserName(userName)
			.enterPassword(password)
			.clickLogin()
			.clickCRMSFA()
			.clickLeadsButton()
			.clickCreateLead()
//			.enterFirstName(firstName)
			.enterLastName(lastName)
			.enterCompName(compName)
			.clickCreateLeadSubmit()
			.validateFirstName(firstName);
	}
}
