package testCases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001Login extends ProjectMethods {
	
	@BeforeTest(groups= {"smoke"})
	public void setData() {
		testName = "TC002_CreateLeads";
		testDesc = "Create A new Lead";
		author = "fariti";
		category = "smoke";
		filePath="LoginData";
	}
	@Test(dataProvider="qa")
	public void login(String uName, String password) {
		new LoginPage().enterUserName(uName).enterPassword(password).clickLogin();
	}
}
