package util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.model.Log;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public abstract class Reporter {
	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public String testName, testDesc, author, category;
	public static ExtentTest logger;
	
	
public void startResult() {
	html=new ExtentHtmlReporter("./reports/result.html");
	html.setAppendExisting(false);	
	extent=new ExtentReports();
	extent.attachReporter(html);
	
}

public void endResult() {
	extent.flush();
}

public void beforeMethod() {
	logger = extent.createTest(testName, testDesc);
	logger.assignAuthor(author);
	logger.assignCategory(category);
}

public void reportStep(String status, String desc) {
	if (status.equalsIgnoreCase("pass")) {
		logger.log(Status.PASS, desc);			
	} else if (status.equalsIgnoreCase("fail")) {
		logger.log(Status.FAIL, desc);			
	}

}

}
