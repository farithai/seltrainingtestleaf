package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement eleSubmit;
	
	
	public CreateLeadPage enterFirstName(String fName) {
		type(eleFirstName, fName);
		return this;
		
	}
	
	public CreateLeadPage enterLastName(String lName) {
		type(eleLastName, lName);
		return this;
	}
	
	public CreateLeadPage enterCompName(String compName) {
		type(eleCompanyName, compName);
		return this;
	}
	
	public ViewLeadPage clickCreateLeadSubmit() {
		click(eleSubmit);
		return new ViewLeadPage();
	}
	
}
