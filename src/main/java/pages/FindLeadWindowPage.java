package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadWindowPage extends ProjectMethods {

	public FindLeadWindowPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//input[@name='id']") WebElement eleLeadId;
	@FindBy(how=How.XPATH, using="//button[text()=Find Leads") WebElement eleFindLead;
	@FindBy(how=How.CLASS_NAME, using="linktext") WebElement eleIdLink;
	@FindBy(how=How.XPATH, using="//input[@name='firstName']") WebElement eleFirstName;
	@FindBy(how=How.XPATH, using="//input[@name='lastName']") WebElement eleLastName;
	@FindBy(how=How.XPATH, using="//input[@name='companyName']") WebElement eleCompName;
	
	
	
	
	
	
	public FindLeadWindowPage enterLeadID(String leadID) {
		type(eleLeadId, leadID);
		return this;
	}
	
	public FindLeadWindowPage enterFirstName(String firstName) {
		type(eleFirstName, firstName);
		return this;
	}
	
	public FindLeadWindowPage enterLastName(String lastName) {
		type(eleLastName, lastName);
		return this;
	}
	
	public FindLeadWindowPage enterCompanyName(String compName) {
		type(eleCompName, compName);
		return this;
	}
	
	public void clickFindLeads() throws InterruptedException {
		click(eleIdLink);
		Thread.sleep(3000);
	}
	
	
	
	public ViewLeadPage clickLeadID() {
		click(eleIdLink);
		return new ViewLeadPage();
		
	}
	
	public MergeLeadPage clickLeadIDWindow() {
		click(eleIdLink);
		switchToWindow(0);
		return new MergeLeadPage();
		
	}
	

}
