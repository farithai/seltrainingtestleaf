package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement eleFirstName;
	@FindBy(how=How.ID, using="Edit") WebElement eleEdit;
	@FindBy(how=How.ID, using="Duplicate Lead") WebElement eleDupLead;
	@FindBy(how=How.ID, using="Delete") WebElement eleDelete;
	
	public ViewLeadPage validateFirstName(String fName) {
		verifyExactText(eleFirstName, fName);
		return this;
		
	}

	public EditLeadPage clickEditLink() {
		click(eleEdit);
		return new EditLeadPage();
		
	}
	
	public MyLeadsPage clickDeleteLink() {
		click(eleDelete);
		return new MyLeadsPage();
		
	}
	
	public DuplicateLeadPage clickDupLeadLink() {
		click(eleDupLead);
		return new DuplicateLeadPage();
		
	}
	
}
