package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {

	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//table[@name='ComboBox_partyIdFrom']/following-sibling::a/img") WebElement eleFromIcon;
	@FindBy(how=How.XPATH, using="//table[@name='ComboBox_partyIdTo']/following-sibling::a/img1") WebElement eleToIcon;//changed img to img1
/*	@FindBy(how=How.XPATH, using="//input[@name='firstName']") WebElement eleFirstNameMerge;
	@FindBy(how=How.XPATH, using="//input[@name='companyName']") WebElement eleCompNameMerge;
	@FindBy(how=How.XPATH, using="//input[@name='lastName']") WebElement eleLastNameMerge;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement elFindLeadsMerge;
	@FindBy(how=How.XPATH, using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]") WebElement eleResultMerge;*/
	@FindBy(how=How.LINK_TEXT, using="Merge") WebElement eleMergeButton;
	
	public FindLeadPage clickFromLeadIcon() {
		click(eleFromIcon);	
		switchToWindow(1);
		driver.manage().window().maximize();
		return new FindLeadPage();
		
	}
	
	public FindLeadPage clickToLeadIcon() {
		click(eleToIcon);	
		switchToWindow(1);
		driver.manage().window().maximize();
		return new FindLeadPage();
		
	}
	
	public MergeLeadPage clickMergeButton() {
		click(eleMergeButton);
		return this; 
				
	}
	
	public ViewLeadPage handleAlert() {
	getAlertText();
	acceptAlert();
	return new ViewLeadPage();

	}

	
}
