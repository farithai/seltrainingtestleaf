package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="username") WebElement eleUserName;
	@FindBy(how=How.ID, using="password") WebElement elePassword;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;
	
	public LoginPage enterUserName(String uName) {
		type(eleUserName, uName);	
		return this;	
	}
	
	public LoginPage enterPassword(String passwd) {
		type(elePassword, passwd);	
		return this;	
	}
	
	public HomePage clickLogin() {
		click(eleLogin);	
		return new HomePage();	
	}
}
