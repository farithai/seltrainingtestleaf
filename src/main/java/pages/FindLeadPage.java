package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods {

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//input[@name='id']") WebElement eleLeadId;
	@FindBy(how=How.XPATH, using="//button[text()=Find Leads") WebElement eleFindLead;
	@FindBy(how=How.CLASS_NAME, using="linktext") WebElement eleIdLink;
	@FindBy(how=How.XPATH, using="//input[@name='firstName']") WebElement eleFirstName;
	@FindBy(how=How.XPATH, using="//input[@name='lastName']") WebElement eleLastName;
	@FindBy(how=How.XPATH, using="//input[@name='companyName']") WebElement eleCompName;
	
	
	
	public FindLeadPage enterLeadID(String leadID) {
		type(eleLeadId, leadID);
		return this;
	}
	
	public FindLeadPage enterFirstNameMerge(String firstName) {
		type(eleFirstName, firstName);
		return this;
	}
	
	public FindLeadPage enterLastNameMerge(String lastName) {
		type(eleLastName, lastName);
		return this;
	}
	
	public FindLeadPage enterCompanyNameMerge(String compName) {
		type(eleCompName, compName);
		return this;
	}
	
	public FindLeadPage clickFindLeads() throws InterruptedException {
		click(eleIdLink);
		Thread.sleep(3000);
		return this;
	}
	
	
	
	public ViewLeadPage clickLeadID() {
		click(eleIdLink);
		return new ViewLeadPage();
		
	}
	
	public MergeLeadPage clickLeadIDWindow() {
		click(eleIdLink);
		switchToWindow(0);
		return new MergeLeadPage();
		
	}
	

}
